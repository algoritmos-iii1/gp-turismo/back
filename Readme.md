# BACK END DA APLICAÇÃO GPTURISMO

## Apropriar do Projeto

git clone https://gitlab.com/algoritmos-iii1/gp-turismo/back/

## Rodar a Aplicação
python app.py

## Rodar o teste unitario
pytest

## Deploy
https://gpturismo.herokuapp.com/

# Routes Principais
## excursoes.route
/excursoes

/excursoes/<int:idExcursoes>

## viagens.route
/viagens

/viagens/<int:idViagens>

/viagens/excursoes/<cidade>
  
## usuarios.routes
/usuarios

/login

/logout
